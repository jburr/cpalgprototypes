"""Handles for selections on objects"""

from __future__ import annotations
from collections import defaultdict
from typing import DefaultDict, List, Optional, Set, Tuple, Union, cast
from typing_extensions import TypeAlias

import boolean
from componentwrapper.component import Component

from cpalggraph.booleanalgebra import ALL_OBJECTS, AND, PRESELECTED_OBJECTS, algebra
from cpalggraph.configurationinfo import ConfigurationInfo
from cpalggraph.datatypes import Dependency, Selection
from cpalggraph.preseldict import PreselDict
from cpalgprototypes.containerdependentmixin import ContainerDependentMixin
from cpalgprototypes.containerhandle import ContainerHandle
from cpalgprototypes.handledescriptor import HandleDescriptor
from cpalgprototypes.propertydescriptor import MISSING, MISSING_TYPE
from cpalgprototypes.prototype import Prototype

ExprType: TypeAlias = Union[str, boolean.Expression]


def preselections_from_input_expression(
    expr: boolean.Expression, container: str, info: ConfigurationInfo
) -> DefaultDict[Dependency, boolean.Expression]:
    """Extract the necessary preselections from an input expression

    We can be a bit smarter when it comes to input selections. For example, in the
    expression A && !B && C, we only need B on objects where A passed and C where A
    passed and B failed.

    In order to do this, we extract the order of the selections from the configuration
    info.

    Parameters
    ----------
    expr : boolean.Expression
        The input expression
    container : str
        The container (nickname) on which the selections are defined
    info : ConfigurationInfo
        The configuration info containing information on the order in which selections
        are defined in the graph.

    Returns
    -------
    DefaultDict[Dependency, boolean.Expression]
        The requirements deduced from this expression
    """
    presels: DefaultDict[Dependency, boolean.Expression] = defaultdict(
        lambda: PRESELECTED_OBJECTS
    )
    # We may need to handle more complex cases than the one in the docstring. We can
    # convert the expression into a well understood input format called the
    # disjunctive normal form. This is guaranteed to be either
    # - A (possibly negated) symbol
    # - An AND or such symbols
    # - An OR of such ANDs

    # The boolean library doesn't give us a type annotation with this guarantee so we
    # have to tell the typing system that we can rely on it
    dnf = cast(
        Union[boolean.Symbol, boolean.NOT, boolean.AND, boolean.OR], algebra.dnf(expr)
    )
    if isinstance(dnf, (boolean.Symbol, boolean.NOT)):
        # Simple case, there is only one symbol
        for symbol in dnf.symbols:
            presels[Dependency(container, str(symbol))] = ALL_OBJECTS
    else:
        # Ensure that we're dealing with a tuple of ANDs
        ands = (
            (dnf,)
            if isinstance(dnf, boolean.AND)
            else cast(Tuple[boolean.AND], dnf.args)
        )
        for and_expr in ands:
            # Go through in order, starting with all the objects of the first selection
            # then refining the requirement as we go step by step
            presel = ALL_OBJECTS
            for subexpr in info.get_symbol_order(container, and_expr):
                for symbol in subexpr.symbols:
                    presels[Dependency(container, str(symbol))] |= presel
                presel &= subexpr
    return presels


class InputSelection(
    HandleDescriptor[ExprType, boolean.Expression], ContainerDependentMixin
):
    """Handle for a selection on input objects"""

    def _convert_set(self, value: ExprType) -> boolean.Expression:
        return value if isinstance(value, boolean.Expression) else algebra.parse(value)

    def requires(self, obj: Prototype) -> Set[Dependency]:
        container = self.assert_parent_container(obj)
        return {
            Dependency(container, str(symbol)) for symbol in self.__get__(obj).symbols
        }

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        super().initialize(obj, component, info)
        component[self.property_name] = info.get_selection_property_value(
            self.assert_parent_container(obj), self.__get__(obj)
        )

    def finalize(
        self,
        obj: Prototype,
        component: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        super().finalize(obj, component, container_preselections, required_output, info)
        container = self.assert_parent_container(obj)
        # Get the overall preselection for this container
        presel = container_preselections[container]
        for dep, sel in preselections_from_input_expression(
            self.__get__(obj), container, info
        ).items():
            required_output.require(dep, presel & sel)


class InputSelectionArray(
    HandleDescriptor[List[ExprType], List[boolean.Expression]], ContainerDependentMixin
):
    """Handle for an array of selections on input objects"""

    def _convert_set(self, value: List[ExprType]) -> List[boolean.Expression]:
        return [
            v if isinstance(v, boolean.Expression) else algebra.parse(v) for v in value
        ]

    def requires(self, obj: Prototype) -> Set[Dependency]:
        container = self.assert_parent_container(obj)
        return {
            Dependency(container, symbol)
            for expr in self.__get__(obj)
            for symbol in expr.symbols
        }

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        super().initialize(obj, component, info)
        container = self.assert_parent_container(obj)
        component[self.property_name] = [
            info.get_selection_property_value(container, expr)
            for expr in self.__get__(obj)
        ]

    def finalize(
        self,
        obj: Prototype,
        component: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        super().finalize(obj, component, container_preselections, required_output, info)

        container = self.assert_parent_container(obj)
        presel = container_preselections[container]
        for expr in self.__get__(obj):
            for dep, sel in preselections_from_input_expression(
                expr, container, info
            ).items():
                required_output.require(dep, presel & sel)


class SequentialInputSelectionArray(InputSelectionArray):
    """Input selection array where each selection is applied sequentially

    This is used to signal to the preselection mechanism that we only need each
    subsequent selection on the objects passing the AND of all preceding selections
    """

    def finalize(
        self,
        obj: Prototype,
        component: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        container = self.assert_parent_container(obj)
        presel = container_preselections[container]
        exprs = self.__get__(obj)

        full_expr: boolean.Expression
        if len(exprs) == 0:
            return
        elif len(exprs) == 1:
            full_expr = exprs[0]
        else:
            full_expr = AND(*exprs)
        for dep, sel in preselections_from_input_expression(
            full_expr, container, info
        ).items():
            required_output.require(dep, presel & sel)


class OutputSelection(
    HandleDescriptor[Union[str, Selection], Selection], ContainerDependentMixin
):
    """Handle for output selections"""

    def __init__(
        self,
        default_value: Union[str, MISSING_TYPE] = MISSING,
        *,
        is_sys_varied: bool = False,
        property_name: Optional[str] = None,
        required: bool = True,
        parent_container: Union[ContainerHandle, str, None] = None,
        literal_container: Optional[str] = None,
        force_no_parent: bool = False,
        preselection: Optional[InputSelection] = None,
        **kwargs,
    ):
        """Create the handle

        Parameters
        ----------
        default_value : Optional[SetT], optional
            An optional default value for the property
        is_sys_varied : bool
            Whether the output aux item is systematically varied
        property_name : Optional[str], optional
            The property name to set. If not supplied will be set by the name of this
            attribute.
        required : bool
            Whether this property must be set for the configuration to be valid
        parent_container : ContainerHandle | str, optional
            Either the container handle for the parent container or the name of that
            container on the owner of this handle, by default None
        literal_container : str | None
            A literal container name to use. If set prefer this over parent_container
        force_no_parent : bool, optional
            Force there to be no parent container assigned, by default False
        """
        super().__init__(
            default_value,
            property_name=property_name,
            required=required,
            parent_container=parent_container,
            literal_container=literal_container,
            force_no_parent=force_no_parent,
            **kwargs,
        )
        self.is_sys_varied = is_sys_varied

    def has_value(self, obj: Prototype) -> bool:
        return super().has_value(obj) and bool(self.__get__(obj).dependency.aux)

    def __set__(self, obj: Prototype, value: Union[str, Selection]) -> None:
        if not isinstance(value, Selection):
            value = Selection(
                dependency=Dependency(self.assert_parent_container(obj), value),
                is_sys_varied=self.is_sys_varied,
            )
        return super().__set__(obj, value)

    def produces(self, obj: Prototype) -> Set[Dependency]:
        return {self.__get__(obj).dependency}

    def produces_selections(self, obj: Prototype) -> List[Selection]:
        return [self.__get__(obj)]

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        info.add_selection(self.__get__(obj))
        component[self.property_name] = info.get_selection_property_value(
            self.__get__(obj).dependency
        )

    def finalize(
        self,
        obj: Prototype,
        component: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        required_output.make_unavailable(self.__get__(obj))


class OutputCharSelection(OutputSelection):
    """Selection handle for cases where the C++ object is just a normal write handle

    In these cases we shouldn't set the ,as_char part of the string, but everything
    else is the same as for an OutputSelection
    """

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        super().initialize(obj, component, info)
        component[self.property_name] = info.get_true_auxdata(
            self.__get__(obj).dependency
        )


# NB The None, None here indicates that the user should never set anything for this on
# the prototype
class Preselection(HandleDescriptor[None, None], ContainerDependentMixin):
    """Handle to fill in a container preselection"""

    def __get__(self, obj: Optional[Prototype], objtype=None):
        if obj is None:
            return self
        else:
            return None

    def __set__(self, obj: Prototype, value: None) -> None:
        pass

    def has_value(self, obj: Prototype) -> bool:
        # Have to return True here otherwise the finalize method will never be called
        # This also means that it doesn't matter if this is marked as required as it's
        # always reported as having a value
        return True

    def copy_prop(self, obj: Prototype, copy: Component) -> None:
        pass

    def finalize(
        self,
        obj: Prototype,
        component: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        container = self.assert_parent_container(obj)
        component[self.property_name] = info.get_selection_property_value(
            container, container_preselections[container]
        )
