"""Algorithm class that also acts as a node in the graph"""

from collections import defaultdict
from typing import Any, DefaultDict, List, Optional, Sequence

import boolean

from componentwrapper.algorithm import Algorithm
from componentwrapper.icreatable import ICreatable
from cpalggraph.booleanalgebra import PRESELECTED_OBJECTS
from cpalggraph.configurationinfo import ConfigurationInfo
from cpalggraph.node import Node
from cpalggraph.preseldict import PreselDict
from cpalgprototypes.prototype import Prototype


class AlgorithmPrototype(Prototype, Node):
    """Prototype for algorithms

    This also acts as a node in the dependency graph
    """

    def __init__(
        self,
        type: Optional[str] = None,
        name: Optional[str] = None,
        priority: Optional[int] = None,
        **properties: Any
    ):
        super().__init__(type, name, **properties)
        self._priority = priority if priority is not None else 0

    def create_component(self) -> Algorithm:
        return Algorithm(self.type, self.name)

    def create_algs(self, info: ConfigurationInfo) -> List[ICreatable]:
        algorithm = self.create_component()
        self.initialize_component(algorithm, info)
        return [algorithm]

    def finalize(
        self,
        algorithms: Sequence[ICreatable],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        assert len(algorithms) == 1 and isinstance(algorithms[0], Algorithm)
        self.finalize_component(
            algorithms[0],
            self._get_container_preselections(required_output, info),
            required_output,
            info,
        )

    @property
    def priority(self) -> int:
        return self._priority

    @property
    def has_job_output(self) -> bool:
        return False

    def _get_container_preselections(
        self, required_output: PreselDict, info: ConfigurationInfo
    ) -> DefaultDict[str, boolean.Expression]:
        """Get the preselections for each container"""
        preselections: DefaultDict[str, boolean.Expression] = defaultdict(
            lambda: PRESELECTED_OBJECTS
        )

        # Assume that, per container, the selection required on the input auxdata is
        # the OR of the selections required on the output objects. This will be correct
        # when the calculations are being done are per-object only.

        # Use the produces_containers to transfer dependencies from parent to child
        produces_containers = self.produces_containers()

        for dep in self.produces():
            container = produces_containers.get(dep.container, dep.container)
            if container is None:
                container = dep.container
            preselections[container] |= required_output[dep]

        # Now simplify them
        for cont, sel in preselections.items():
            preselections[cont] = sel.simplify()

        return preselections

    def __eq__(self, other: object) -> bool:
        return Prototype.__eq__(self, other)
