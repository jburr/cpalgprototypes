"""Prototype objects for the cpalggraph module"""

__version__ = "0.1.0"

import enum

from cpalgprototypes.enumproperty import EnumValueListProperty, EnumValueProperty
from cpalgprototypes.prototype import ServicePrototype, ToolPrototype
from cpalgprototypes.prototypedecorator import property_maker
from cpalgprototypes.prototypehandles import (
    PrivateToolHandle,
    PrivateToolHandleArray,
    ServiceHandle,
    ServiceHandleArray,
)
from cpalgprototypes.unitproperty import QuantityListProperty, QuantityProperty
from cpalgprototypes.units import QuantityBase  # type: ignore

property_maker.register(enum.Enum, EnumValueProperty)
property_maker.register_list(enum.Enum, EnumValueListProperty)
property_maker.register(ToolPrototype, lambda _, default: PrivateToolHandle(default))  # type: ignore
property_maker.register_list(  # type: ignore
    ToolPrototype, lambda _, default: PrivateToolHandleArray(default)
)
property_maker.register(ServicePrototype, lambda _, default: ServiceHandle(default))  # type: ignore
property_maker.register_list(  # type: ignore
    ServicePrototype, lambda _, default: ServiceHandleArray(default)
)
property_maker.register(QuantityBase, QuantityProperty)
property_maker.register_list(QuantityBase, QuantityListProperty)
