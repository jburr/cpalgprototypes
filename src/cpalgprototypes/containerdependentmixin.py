"""Mixin class for handles that depend on some parent container"""

from __future__ import annotations
import abc
from typing import Optional, Union
from cpalgprototypes.containerhandle import ContainerHandle
from cpalgprototypes.prototype import Prototype


class ContainerDependentMixin(abc.ABC):
    """Mixin class for handles that depend on some parent container

    This is mainly intended for output containers and aux handles
    """

    @abc.abstractproperty
    def property_name(self) -> str:
        """The property name"""

    def __init__(
        self,
        *args,
        parent_container: Union[ContainerHandle, str, None] = None,
        literal_container: Optional[str] = None,
        force_no_parent: bool = False,
        **kwargs,
    ) -> None:
        """Create the handle

        Parameters
        ----------
        parent_container : ContainerHandle | str, optional
            Either the container handle for the parent container or the name of that
            container on the owner of this handle, by default None
        literal_container : str | None
            A literal container name to use. If set prefer this over parent_container
        force_no_parent : bool, optional
            Force there to be no parent container assigned, by default False
        """
        super().__init__(*args, **kwargs)
        self._parent_container = parent_container
        self._literal_container = literal_container
        self._force_no_parent = force_no_parent

    def get_parent_container(self, obj: Prototype) -> Optional[str]:
        """Get the parent container

        The priority goes as follows
        1. If force_no_parent is set, return None
            2. If set, use the literal_container provided to the constructor
            3. If set, use the handle/parent name provided to the constructor
            4. Otherwise use the owner's default container
        """
        if self._force_no_parent:
            return None
        if self._literal_container is not None:
            return self._literal_container
        if self._parent_container is not None:
            if isinstance(self._parent_container, ContainerHandle):
                return self._parent_container.__get__(obj)
            else:
                return getattr(obj, self._parent_container)
        return obj.get_default_container()

    def assert_parent_container(self, obj: Prototype) -> str:
        """Get the parent container, raising an exception if it is not present"""
        container = self.get_parent_container(obj)
        assert (
            container is not None
        ), f"Cannot determine container for {obj}.{self.property_name}"
        return container
