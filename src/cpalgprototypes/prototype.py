"""Base class for prototypes

For various reasons, the values that are attached to the objects provided to the graph
for properties are not the same as the final components produced by the job (for
example, no preselections are set on the objects given to the graph, container and aux
nicknames are replaced, etc). In general, this also allows us to set more physically
meaningful types on the prototypes and then rely on the descriptors to convert these to
the correct underlying type for the C++ property.

The final components also should not obey the Node interface requirements, whereas the
algorithm-like objects given to the graph should. However, we still want to preeserve
the simple node <-> algorithm mapping which is mostly a good model.

This motivates having another class which combines the property-like description of the
Component classes with the full dependency mechanism. As these are preliminary objects
from which the final versions are derived, we call these Prototypes.
"""

from __future__ import annotations

import abc
import collections.abc
import copy
import logging
from typing import (
    Any,
    DefaultDict,
    Dict,
    Iterator,
    List,
    Optional,
    TYPE_CHECKING,
    Set,
    Tuple,
)

import boolean

from componentwrapper.component import Component
from componentwrapper.service import Service
from componentwrapper.tool import Tool
from cpalggraph.configurationinfo import ConfigurationInfo
from cpalggraph.datatypes import Dependency, Selection
from cpalggraph.preseldict import PreselDict
from cpalggraph.node import merge_produces_containers

if TYPE_CHECKING:
    from cpalgprototypes.propertydescriptor import PropertyDescriptor
    from cpalgprototypes.handledescriptor import HandleDescriptor

log = logging.getLogger(__name__)


class Prototype(collections.abc.MutableMapping[str, Any]):
    """Base class for prototypes"""

    #: The C++ type to create. Will be deduced from the class name if not present
    _cxx_type: str

    @classmethod
    def get_default_type(cls) -> str:
        """Get the default C++ type for this object

        This is used if no type argument is provided to the Prototype constructor.

        If the derived class has a '_cxx_type' field then the value in that will be
        used. Otherwise it will be deduced from the class name, replacing any double
        underscores with '::'.
        """
        try:
            return cls._cxx_type
        except AttributeError:
            return cls.__name__.replace("__", "::")

    @classmethod
    def get_property_descriptors(cls) -> Tuple[Tuple[str, PropertyDescriptor], ...]:
        """Get all of the property descriptors attached to this class"""
        from cpalgprototypes.propertydescriptor import PropertyDescriptor

        return tuple(
            (x, attr)
            for x in dir(cls)
            if isinstance(attr := getattr(cls, x), PropertyDescriptor)
        )

    @classmethod
    def get_handle_descriptors(cls) -> Tuple[Tuple[str, HandleDescriptor], ...]:
        """Get all of the property descriptors attached to this class"""
        from cpalgprototypes.handledescriptor import HandleDescriptor

        return tuple(
            (x, attr)
            for x in dir(cls)
            if isinstance(attr := getattr(cls, x), HandleDescriptor)
        )

    def __init__(
        self, type: Optional[str] = None, name: Optional[str] = None, **properties: Any
    ):
        if type is None:
            type = self.get_default_type()
        if name is None:
            name = type.rpartition("::")[2]
        self._type = type
        self._name = name
        #: The parent of this prototype (if any)
        self._parent: Optional[Prototype] = None
        #: When retrieving containers from the parent allow renaming them
        self._rename_parent_containers: Dict[Optional[str], Optional[str]] = {}
        self.__properties: Dict[str, Any] = {}

        # Prefer to set properties from their descriptors
        for _, prop_desc in self.get_property_descriptors():
            try:
                value = properties.pop(prop_desc.property_name)
            except KeyError:
                if prop_desc.has_default:
                    prop_desc.__set__(self, prop_desc._default_value)
            else:
                prop_desc.__set__(self, value)
        # Any remaining get set as normal k, v pairs
        self.update(properties)

    @property
    def type(self) -> str:
        """The component concrete C++ type"""
        return self._type

    @property
    def name(self) -> str:
        """The component name"""
        return self._name

    @property
    def type_and_name(self) -> str:
        return f"{self.type}/{self.name}"

    @property
    def parent(self) -> Optional[Prototype]:
        """The parent of this object (if any)"""
        return self._parent

    @parent.setter
    def parent(self, value: Optional[Prototype]) -> None:
        self.set_parent(value, {})

    def set_parent(
        self,
        parent: Optional[Prototype],
        rename_containers: Dict[Optional[str], Optional[str]] = {},
    ):
        """Set the parent of this prototype

        Parameters
        ----------
        parent : Optional[Prototype]
            The parent
        rename_containers : Dict[Optional[str], Optional[str]], optional
            Rename containers from the parent, by default {}. This is used by the
            get_named_container method
        """
        self._parent = parent
        self._rename_parent_containers = copy.copy(rename_containers)

    """ --- Component interface --- """

    @abc.abstractmethod
    def create_component(self) -> Component:
        """Create the final component object represented by this prototype"""

    def initialize_component(self, component: Component, info: ConfigurationInfo):
        """Initialize the created component

        Parameters
        ----------
        component : Component
            The object returned by create_component
        info : ConfigurationInfo
            Information on the job configuration which is built up through successive
            initialize_component calls on each node in the execution order
        """
        # Process all property descriptors
        for name, prop in self.get_property_descriptors():
            if prop.has_value(self):
                prop.copy_prop(self, component)
                if isinstance(prop, HandleDescriptor):
                    prop.initialize(self, component, info)
            elif prop.required:
                log.error(f"Required attribute {name} of {self.type_and_name} not set")
                raise AttributeError(name)

        # Now copy the rest of the properties, skipping any that the descriptors have
        # already handled
        for k, v in self.items():
            if k not in component:
                component[k] = copy.deepcopy(v)

    def finalize_component(
        self,
        component: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        """Finalize the component created by create_component

        This handles updating the required_output dictionary so that upstream nodes
        know which preselections to apply.

        Parameters
        ----------
        component : Component
            The component created by create_component
        container_preselections : DefaultDict[str, boolean.Expression]
            Preselections per container. These are used to determine the required input
            objects
        required_output : PreselDict
            The list of required outputs for each auxitem
        info : ConfigurationInfo
            The ConfigurationInfo object that was built up through the
            initialize_component calls
        """
        for _, handle in self.get_handle_descriptors():
            if handle.has_value(self):
                handle.finalize(
                    self, component, container_preselections, required_output, info
                )

    """ --- Container interface --- """

    def get_default_container(self) -> Optional[str]:
        """Get the default container to be used by handles on this or on a child

        Returns
        -------
        Optional[str]
            The default container if one could be determined
        """
        from cpalgprototypes.containerhandle import ContainerHandle

        container_handles = tuple(
            h
            for _, h in self.get_handle_descriptors()
            if isinstance(h, ContainerHandle)
        )
        default_handles = tuple(h for h in container_handles if h.is_default_container)
        # It's an error for a class definition to contain more than one handle which
        # considers itself the default
        assert len(default_handles) <= 1
        if len(default_handles) == 1:
            return default_handles[0].__get__(self)
        # If we've specified a way to access a default container from the parent, use
        # that
        if self._parent:
            return self._parent.get_named_container(
                self._rename_parent_containers.get(None, None)
            )

        # If we've got here, nothing else has worked, return None
        return None

    def assert_default_container(self) -> str:
        """Get the default container, raising an exception if one cannot be found"""
        container = self.get_default_container()
        assert container
        return container

    def get_named_container(self, name: Optional[str]) -> Optional[str]:
        """Get the nickname for a named container type

        This is similar to get_default_container but for the rare cases where a parent
        processes many different containers.

        For these cases, a parent tool/algorithm can specialise its get_named_containers
        method to ensure that each child gets the correct containers.

        Parameters
        ----------
        name : str | None
            The name of the handle on the parent
        """
        if name is None:
            return self.get_default_container()
        try:
            return getattr(self, name)
        except AttributeError:
            if self._parent:
                return self._parent.get_named_container(
                    self._rename_parent_containers.get(name, name)
                )
            else:
                return None

    def assert_named_container(self, name: Optional[str]) -> str:
        """Get the named container, raising an exception if one cannot be found"""
        container = self.get_named_container(name)
        assert container
        return container

    """ --- Dependency Interface --- """

    def produces(self) -> Set[Dependency]:
        return set().union(
            *(
                handle.produces(self)
                for _, handle in self.get_handle_descriptors()
                if handle.has_value(self)
            )
        )

    def produces_selections(self) -> List[Selection]:
        return sum(
            (
                handle.produces_selections(self)
                for _, handle in self.get_handle_descriptors()
                if handle.has_value(self)
            ),
            [],
        )

    def produces_containers(self) -> Dict[str, Optional[str]]:
        return merge_produces_containers(
            handle.produces_containers(self)
            for _, handle in self.get_handle_descriptors()
            if handle.has_value(self)
        )

    def requires(self) -> Set[Dependency]:
        return set().union(
            *(
                handle.requires(self)
                for _, handle in self.get_handle_descriptors()
                if handle.has_value(self)
            )
        )

    """ --- Mapping interface --- """

    def __getitem__(self, key: str) -> Any:
        return self.__properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.__properties[key] = value
        if isinstance(value, Prototype):
            value._parent = self

    def __delitem__(self, key: str) -> None:
        del self.__properties[key]

    def __iter__(self) -> Iterator[str]:
        return iter(self.__properties)

    def __len__(self) -> int:
        return len(self.__properties)


class ToolPrototype(Prototype):
    """Prototype class for tools"""

    def create_component(self) -> Tool:
        return Tool(self.type, self.name)


class ServicePrototype(Prototype):
    """Prototype class for services"""

    def create_component(self) -> Service:
        return Service(self.type, self.name)
