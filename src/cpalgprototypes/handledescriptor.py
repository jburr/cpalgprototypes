"""Base class for properties that also interact with the dependency mechanism"""

from __future__ import annotations
from typing import DefaultDict, Dict, List, Optional, Set, Union, TYPE_CHECKING

import boolean

from componentwrapper.component import Component
from cpalggraph.configurationinfo import ConfigurationInfo
from cpalggraph.datatypes import Dependency, Selection
from cpalggraph.preseldict import PreselDict
from cpalgprototypes.propertydescriptor import (
    PropertyDescriptor,
    SetT,
    GetT,
    MISSING,
    MISSING_TYPE,
)

if TYPE_CHECKING:
    from cpalgprototypes.prototype import Prototype


class HandleDescriptor(PropertyDescriptor[SetT, GetT]):
    """Property descriptor that interacts with the dependency mechanism"""

    def __init__(
        self,
        default_value: Union[SetT, MISSING_TYPE] = MISSING,
        *,
        property_name: Optional[str] = None,
        required: bool = False,
        **kwargs,
    ):
        # NB: Handles are assumed to be required
        super().__init__(
            default_value, property_name=property_name, required=required, **kwargs
        )

    def produces(self, obj: Prototype) -> Set[Dependency]:
        """The data produced by this handle"""
        return set()

    def produces_selections(self, obj: Prototype) -> List[Selection]:
        """The selections produced by this handle"""
        return []

    def produces_containers(self, obj: Prototype) -> Dict[str, Optional[str]]:
        """The containers produced by this handle"""
        return {}

    def requires(self, obj: Prototype) -> Set[Dependency]:
        """The data required by this handle"""
        return set()

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        """Initialize the component and update the configuration info

        Parameters
        ----------
        obj : Prototype
            The prototype object instance
        component : Component
            The created component instance
        info : ConfigurationInfo
            The job's configuration info
        """

    def finalize(
        self,
        obj: Prototype,
        component: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        """Finalize the component, updating the preseldict

        Parameters
        ----------
        obj : Prototype
            The prototype object instance
        copy : Component
            The created component object
        container_preselections : DefaultDict[str, boolean.Expression]
            Preselections per container. These are used to determine the required input
            objects
        required_output : PreselDict
            The preselection dictionary
        info : ConfigurationInfo
            The job's configuration info
        """
        for dep in self.produces(obj):
            required_output.drop(dep)
        for dep in self.requires(obj):
            required_output.require(dep, container_preselections[dep.container])
