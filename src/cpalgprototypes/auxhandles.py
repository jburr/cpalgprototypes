"""Handle objects for input/output auxdata"""

from __future__ import annotations
from typing import List, Optional, Set, Tuple, Union
from componentwrapper.component import Component
from cpalggraph.configurationinfo import ConfigurationInfo
from cpalggraph.datatypes import Dependency
from cpalggraph.syspattern import add_sys_pattern
from cpalgprototypes.containerdependentmixin import (
    ContainerDependentMixin,
)
from cpalgprototypes.containerhandle import ContainerHandle
from cpalgprototypes.handledescriptor import HandleDescriptor
from cpalgprototypes.propertydescriptor import MISSING, MISSING_TYPE
from cpalgprototypes.prototype import Prototype


class InputAux(HandleDescriptor[str, str], ContainerDependentMixin):
    """Handle for an input auxdata item"""

    def dependency(self, obj: Prototype) -> Dependency:
        """Helper that returns the created dependency"""
        return Dependency(self.assert_parent_container(obj), self.__get__(obj))

    def requires(self, obj: Prototype) -> Set[Dependency]:
        return {self.dependency(obj)}

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        component[self.property_name] = info.get_true_auxdata(self.dependency(obj))


class InputAuxArray(HandleDescriptor[List[str], List[str]], ContainerDependentMixin):
    """Handle representing a list of input aux items from the same container"""

    def dependencies(self, obj: Prototype) -> Tuple[Dependency, ...]:
        """Helper that returns all the created dependencies"""
        container = self.assert_parent_container(obj)
        return tuple(Dependency(container, aux) for aux in self.__get__(obj))

    def requires(self, obj: Prototype) -> Set[Dependency]:
        return set(self.dependencies(obj))

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        component[self.property_name] = [
            info.get_true_auxdata(dep) for dep in self.dependencies(obj)
        ]


class OutputAux(HandleDescriptor[str, str], ContainerDependentMixin):
    """Handle representing an output item"""

    def __init__(
        self,
        default_value: Union[str, MISSING_TYPE] = MISSING,
        *,
        is_sys_varied: bool = False,
        property_name: Optional[str] = None,
        required: bool = True,
        parent_container: Union[ContainerHandle, str, None] = None,
        literal_container: Optional[str] = None,
        force_no_parent: bool = False,
        **kwargs,
    ):
        """Create the handle

        Parameters
        ----------
        default_value : Optional[SetT], optional
            An optional default value for the property
        is_sys_varied : bool
            Whether the output aux item is systematically varied
        property_name : Optional[str], optional
            The property name to set. If not supplied will be set by the name of this
            attribute.
        required : bool
            Whether this property must be set for the configuration to be valid
        parent_container : ContainerHandle | str, optional
            Either the container handle for the parent container or the name of that
            container on the owner of this handle, by default None
        literal_container : str | None
            A literal container name to use. If set prefer this over parent_container
        force_no_parent : bool, optional
            Force there to be no parent container assigned, by default Fals
        """
        super().__init__(
            default_value,
            property_name=property_name,
            required=required,
            parent_container=parent_container,
            literal_container=literal_container,
            force_no_parent=force_no_parent,
            **kwargs,
        )
        self.is_sys_varied = is_sys_varied

    def dependency(self, obj: Prototype) -> Dependency:
        """Helper that returns the created dependency"""
        return Dependency(self.assert_parent_container(obj), self.__get__(obj))

    def produces(self, obj: Prototype) -> Set[Dependency]:
        return {self.dependency(obj)}

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        auxname = (
            add_sys_pattern(self.__get__(obj))
            if self.is_sys_varied
            else self.__get__(obj)
        )
        info.set_true_auxdata(self.dependency(obj), auxname)
        component[self.property_name] = auxname
