"""Decorator to help create prototypes"""

from __future__ import annotations
import copy
import sys
from typing import (
    Any,
    Callable,
    ClassVar,
    Dict,
    List,
    Optional,
    Type,
    TypeVar,
    Union,
    get_args,
    get_origin,
    get_type_hints,
    overload,
)
from typing_extensions import TypeAlias

from cpalgprototypes.propertydescriptor import (
    MISSING,
    MISSING_TYPE,
    PropertyDescriptor,
    SetT,
)
from cpalgprototypes.prototype import Prototype


T = TypeVar("T")

#: Alias for functions that create a property
PropertyFunc: TypeAlias = Callable[
    [Type, Union[MISSING_TYPE, SetT]], PropertyDescriptor[SetT, T]
]
PrototypeT = TypeVar("PrototypeT", bound=Type[Prototype])


class PropertyMaker:
    """Registry of conversions from type annotations to property types"""

    def __init__(self) -> None:
        self._functions: Dict[Type, PropertyFunc] = {}
        self._list_functions: Dict[Type, PropertyFunc] = {}

    def register(
        self, type_: Type[T], func: PropertyFunc[SetT, T], overwrite: bool = False
    ):
        """Register a new conversion function

        Parameters
        ----------
        type_ : Type[T]
            The type to convert
        func : PropertyFunc[SetT, T]
            THe function that maps from an optional default value to a descriptor
        overwrite : bool, optional
            If there is already a conversion function then overwrite it. Otherwise this
            would raise a KeyError, by default False
        """
        if not overwrite and type_ in self._functions:
            raise KeyError(type_)
        self._functions[type_] = func

    def register_list(
        self,
        type_: Type[T],
        func: PropertyFunc[SetT, List[T]],
        overwrite: bool = False,
    ):
        """Register a new conversion function for list types

        Parameters
        ----------
        type_ : Type[T]
            The type to convert
        func : PropertyFunc[SetT, T]
            THe function that maps from an optional default value to a descriptor
        overwrite : bool, optional
            If there is already a conversion function then overwrite it. Otherwise this
            would raise a KeyError, by default False
        """
        if not overwrite and type_ in self._list_functions:
            raise KeyError(type_)
        self._list_functions[type_] = func

    def make_property(
        self, type_: Type[T], default: Union[MISSING_TYPE, T] = MISSING
    ) -> PropertyDescriptor:
        """Convert a type annotation to a property descriptor

        The annotated type_'s mro is searched for the first type that matches one that
        has beeen registered. If none is found, a simple PropertyDescriptor is returned

        Parameters
        ----------
        type_ : Type[T]
            The type annotation
        default : MISSING_TYPE | T, optional
            The default value, by default MISSING

        Returns
        -------
        PropertyDescriptor
            The resolved property descriptor
        """
        # Go through the type's MRO to see if we have a conversion for it
        for cls in type_.mro():
            try:
                f = self._functions[cls]
            except KeyError:
                continue
            return f(type_, default)
        # If we reach here we have no specialised implementation, just return a simple
        # PropertyDescriptor
        return PropertyDescriptor[T, T](default_value=default)

    def make_list_property(
        self, type_: Type[T], default: Union[MISSING_TYPE, List[T]] = MISSING
    ) -> PropertyDescriptor:
        """Convert a type annotation to a property descriptor

        The annotated type_'s mro is searched for the first type that matches one that
        has beeen registered. If none is found, a simple PropertyDescriptor is returned.

        This version is called when the annotation is list[T]

        Parameters
        ----------
        type_ : Type[T]
            The type annotation
        default : MISSING_TYPE | list[T], optional
            The default value, by default MISSING

        Returns
        -------
        PropertyDescriptor
            The resolved property descriptor
        """
        # Go through the type's MRO to see if we have a conversion for it
        for cls in type_.mro():
            try:
                f = self._list_functions[cls]
            except KeyError:
                continue
            return f(type_, default)
        # If we reach here we have no specialised implementation, just return a simple
        # PropertyDescriptor
        return PropertyDescriptor[List[T], List[T]](default_value=default)


#: Global instance of the property maker
property_maker = PropertyMaker()


@overload
def prototype(cls: PrototypeT) -> PrototypeT:
    ...


@overload
def prototype(
    *, property_maker: PropertyMaker = property_maker
) -> Callable[[PrototypeT], PrototypeT]:
    ...


def prototype(
    cls: Optional[Type[Prototype]] = None,
    *,
    property_maker: PropertyMaker = property_maker,
):
    """Decorate a prototype

    The annotations of the decorated class will be processed (similarly to the
    dataclass decorator). If they match a type registered in the property_maker the
    function registered in the maker will be used. Otherwise a default property
    descriptor will be used.

    Parameters
    ----------
    cls : Optional[Type[Prototype]], optional
        The type to modify, by default None
    property_maker : PropertyMaker, optional
        The property maker to use, by default property_maker

    Returns
    -------
    The modified type
    """

    # This setup is to allow the decorator to be used as @prototype or as
    # @prototype(property_maker=X)
    def wrap(cls_):
        return _process_class(cls_, property_maker)

    return wrap if cls is None else wrap(cls)


def _process_class(cls: PrototypeT, property_maker: PropertyMaker) -> PrototypeT:
    # A little bit of evil copied from dataclasses: get the correct environment for
    # our execution
    globals: Dict[str, Any] = {}
    if cls.__module__ in sys.modules:
        globals = copy.copy(sys.modules[cls.__module__].__dict__)

    # Go through the annotations and convert them to properties where appropriate
    for attr_name, hint in get_type_hints(cls, globalns=globals).items():
        # Skip any that are class vars
        if get_origin(hint) is ClassVar:
            continue
        default_value = getattr(cls, attr_name, MISSING)
        # Also skip any that are already descriptors
        if _is_descriptor(default_value):
            continue
        if get_origin(hint) is list:
            prop = property_maker.make_list_property(get_args(hint)[0], default_value)
        else:
            prop = property_maker.make_property(hint, default_value)
        setattr(cls, attr_name, prop)
        prop.__set_name__(cls, attr_name)
    return cls


def _is_descriptor(obj: Any) -> bool:
    # A descriptor is any object with one of these three magic methods
    return any(hasattr(obj, x) for x in ("__get__", "__set__", "__delete__"))
