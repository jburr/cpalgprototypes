# type: ignore
"""Module that auto-generates types representing units"""

# NB: type checking turned off because pint's typing is off

from __future__ import annotations

from numbers import Real
from typing import Any, Literal, Optional, Type, TypeVar, cast, overload

import pint


class QuantityMeta(type):
    """Metaclass for quantities"""

    @overload
    def __mul__(self, other: Real) -> QuantityBase:
        ...

    @overload
    def __mul__(self, other: QuantityBase) -> QuantityBase:
        ...

    @overload
    def __mul__(self, other: Type[QuantityBase]) -> Type[QuantityBase]:
        ...

    def __mul__(self_, other: Any) -> Any:
        self = cast(QuantityBase, self_)  # type: ignore[assignment]
        if isinstance(other, type) and issubclass(other, QuantityBase):
            return source.create_class(self._unit() * other._unit())
        elif isinstance(other, QuantityBase):
            quantity = self._unit() * other.quantity
            return source.create_class(quantity.units)(quantity.magnitude)
        elif isinstance(other, Real):
            return self_(other)
        else:
            return NotImplemented

    @overload
    def __rmul__(self, other: Real) -> QuantityBase:
        ...

    @overload
    def __rmul__(self, other: QuantityBase) -> QuantityBase:
        ...

    @overload
    def __rmul__(self, other: Type[QuantityBase]) -> Type[QuantityBase]:
        ...

    def __rmul__(self, other: Any) -> Any:
        return self * other

    @overload
    def __truediv__(self, other: Real) -> QuantityBase:
        ...

    @overload
    def __truediv__(self, other: QuantityBase) -> QuantityBase:
        ...

    @overload
    def __truediv__(self, other: Type[QuantityBase]) -> Type[QuantityBase]:
        ...

    def __truediv__(self_, other: Any) -> Any:
        self = cast(QuantityBase, self_)  # type: ignore[assignment]
        if isinstance(other, type) and issubclass(other, QuantityBase):
            return source.create_class(self._unit() / other._unit())
        elif isinstance(other, QuantityBase):
            quantity = self._unit() / other.quantity
            return source.create_class(quantity.units)(quantity.magnitude)
        elif isinstance(other, Real):
            return self_(1 / other)
        else:
            return NotImplemented

    @overload
    def __rtruediv__(self, other: Literal[1]) -> Type[QuantityBase]:
        ...

    @overload
    def __rtruediv__(self, other: Real) -> QuantityBase:
        ...

    @overload
    def __rtruediv__(self, other: QuantityBase) -> QuantityBase:
        ...

    @overload
    def __rtruediv__(self, other: Type[QuantityBase]) -> Type[QuantityBase]:
        ...

    def __rtruediv__(self_, other: Any) -> Any:
        self = cast(QuantityBase, self_)  # type: ignore[assignment]
        if isinstance(other, type) and issubclass(other, QuantityBase):
            return source.create_class(other._unit() / self._unit())
        elif isinstance(other, QuantityBase):
            quantity = other.quantity / self._unit()
            return source.create_class(quantity.units)(quantity.magnitude)
        elif isinstance(other, int) and other == 1:
            return source.create_class((1 / self._unit()).units)
        elif isinstance(other, Real):
            return other * (1 / self_)
        else:
            return NotImplemented

    def __pow__(self_, other: int) -> Type[QuantityBase]:
        self = cast(QuantityBase, self_)
        return source.create_class(self._unit() ** other)


QuantityT = TypeVar("QuantityT", bound="QuantityBase")


class QuantityBase:
    """Physical quantity with a fixed unit"""

    @classmethod
    def _unit(cls) -> pint.Unit:
        """The unit for this class"""

    def __init__(self, value: Real | pint.Quantity | str | QuantityBase) -> None:
        self._quantity: pint.Quantity
        if isinstance(value, Real):
            self._quantity = value * self.unit
        elif isinstance(value, QuantityBase):
            self._quantity = value.quantity.to(self.unit)
        else:
            if isinstance(value, str):
                value = source.registry.Quantity(value)
                if value.dimensionless:
                    value *= self.unit
            self._quantity = value.to(self.unit)

    @classmethod
    def parse(cls: Type["QuantityT"], expression: str) -> "QuantityT":
        """Parse a string into a value with the correct units"""
        quantity: pint.Quantity = source.registry.parse_expression(expression)
        return cls(quantity)

    @property
    def quantity(self) -> pint.Quantity:
        return self._quantity

    @property
    def unit(self) -> pint.Unit:
        return type(self)._unit()

    @property
    def magnitude(self) -> Real:
        return self._quantity.magnitude

    def __str__(self) -> str:
        return str(self.quantity)

    def __mul__(self, other: Any) -> QuantityBase:
        if isinstance(other, QuantityBase):
            other = other.quantity
        quantity = self.quantity * other
        return source.create_class(quantity.units)(quantity.magnitude)

    def __rmul__(self, other: Any) -> QuantityBase:
        return self * other

    def __truediv__(self, other: Any) -> QuantityBase:
        if isinstance(other, QuantityBase):
            other = other.quantity
        quantity = self.quantity / other
        return source.create_class(quantity.units)(quantity.magnitude)

    def __rtruediv__(self, other: Any) -> QuantityBase:
        if isinstance(other, QuantityBase):
            other = other.quantity
        quantity = other / self.quantity
        return source.create_class(quantity.units)(quantity.magnitude)


class UnitSource:
    """Class to generate unit classes on the fly"""

    def __init__(self, registry: Optional[pint.UnitRegistry] = None) -> None:
        if registry is None:
            registry = pint.UnitRegistry()
        self.__registry = registry
        self.__defined: dict[str, Type[QuantityBase]] = {}

    @property
    def registry(self) -> pint.UnitRegistry:
        """The registry being used"""
        return self.__registry

    def create_class(
        self, unit: pint.Unit, name: Optional[str] = None
    ) -> Type[QuantityBase]:
        """Create a new class

        Parameters
        ----------
        unit : pint.Unit
            The base unit to use
        name : Optional[str]
            The name of the class to create

        Raises
        ------
        ValueError
            A class with that name already exists and does not match
        """
        if name is None:
            name_list: list[str] = []
            for unit_name, power in unit._units.items():
                symbol: str = self.registry.get_symbol(unit_name)
                if power < 0:
                    symbol = "inv" + symbol
                    power = -power
                if power == 1:
                    name_list.append(symbol)
                else:
                    name_list.append(symbol + str(power))
            name = "_".join(name_list)

        try:
            existing = self.__defined[name]
        except KeyError:
            pass
        else:
            if existing._unit() != unit:
                raise ValueError(name)
            return existing

        def _unit(cls):
            return unit

        return QuantityMeta(  # type: ignore[return-value]
            name,
            (QuantityBase,),
            {"_unit": classmethod(_unit)},
        )

    def __getattr__(self, name: str):
        # We'll use this to replace the module so allow it access to the module's
        # globals
        if name in globals():
            return globals()[name]
        try:
            super().__getattribute__(name)
        except AttributeError:
            try:
                return self.create_class(getattr(self.registry, name), name)
            except pint.errors.UndefinedUnitError:
                raise AttributeError(name)

    @property
    def __all__(self) -> list[str]:
        return ["QuantityMeta", "QuantityBase", "QuantityT"] + list(self.__defined)


source = UnitSource()


# Slightly evil hack to make python think that 'source' is the module.
# Means that you can do
# from cpalgnodes.units import GeV and it will just work
import sys

sys.modules[__name__] = source  #  type: ignore[assignment]
