"""Handle for an input container"""

from __future__ import annotations
from typing import Set
from componentwrapper.component import Component
from cpalggraph.datatypes import Dependency
from cpalgprototypes.containerhandle import ContainerHandle
from cpalggraph.configurationinfo import ConfigurationInfo

from cpalgprototypes.prototype import Prototype


class InputContainer(ContainerHandle):
    """Handle describing an input container to the prototype"""

    def requires(self, obj: Prototype) -> Set[Dependency]:
        return {Dependency(self.__get__(obj), None)}

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        super().initialize(obj, component, info)
        component[self.property_name] = info.get_current_container_name(
            self.__get__(obj)
        )
