"""Classes to allow setting an enum value as a property"""

from __future__ import annotations
import enum
from typing import Any, List, Optional, Type, TypeVar, Union

from cpalgprototypes.propertydescriptor import PropertyDescriptor, MISSING, MISSING_TYPE

EnumT = TypeVar("EnumT", bound=enum.Enum)


class NameEnum(enum.Enum):
    """Enum subclass where enum values (if set by auto) are the same as their names

    If an enum member name starts with a leading underscore it will be removed in the
    value. This is because the class assumes that the underscore was added to get
    around python naming rules - e.g. the desired name started with a number.

    Therefore

    class MyEnum(NameEnum):
        _1A = enum.auto()
        Other = enum.auto()

    is equivalent to

    class MyEnum(enum.Enum):
        _1A = "1A"
        Other = "Other"
    """

    @staticmethod
    def _generate_next_value_(name: str, start, count, last_values) -> str:
        # generate next value is used wherever 'enum.auto' is called. By setting it
        # like this, the value of the enum is set to its name so property converters
        # will behave in the way we expect
        if name.startswith("_"):
            return name[1:]
        else:
            return name


class EnumValueProperty(PropertyDescriptor[Any, EnumT]):
    """Property descriptor for enums

    This allows for supplying the value type of the enum (e.g. str/int) and the
    descriptor ensures that it is a valid value. When being passed to the component the
    underlying enum value is set.
    """

    def __init__(
        self,
        enum_type: Type[EnumT],
        default_value: Union[EnumT, MISSING_TYPE] = MISSING,
        *,
        property_name: Optional[str] = None,
        required: bool = False,
    ):
        self._enum_type = enum_type
        super().__init__(default_value, property_name=property_name, required=required)

    @property
    def enum_type(self) -> Type[EnumT]:
        """The type of enum"""
        return self._enum_type

    def _convert_set(self, value: Any) -> EnumT:
        if isinstance(value, self.enum_type):
            return value
        else:
            return self.enum_type(value)

    def _convert_copy(self, value: EnumT) -> Any:
        return value.value


class EnumValueListProperty(PropertyDescriptor[List[Any], List[EnumT]]):
    """Property descriptor for lists of enum values

    This allows for supplying the value type of the enum (e.g. str/int) and the
    descriptor ensures that it is a valid value. When being passed to the component the
    underlying enum value is set.
    """

    def __init__(
        self,
        enum_type: Type[EnumT],
        default_value: Union[List[EnumT], MISSING_TYPE] = MISSING,
        *,
        property_name: Optional[str] = None,
        required: bool = False,
    ):
        self._enum_type = enum_type
        super().__init__(default_value, property_name=property_name, required=required)

    @property
    def enum_type(self) -> Type[EnumT]:
        """The type of enum"""
        return self._enum_type

    def _convert_set(self, value: List[Any]) -> List[EnumT]:
        return [
            v if isinstance(v, self.enum_type) else self.enum_type(v) for v in value
        ]

    def _convert_copy(self, value: List[EnumT]) -> List[Any]:
        return [v.value for v in value]
