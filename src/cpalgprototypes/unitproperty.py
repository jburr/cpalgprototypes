from __future__ import annotations
from numbers import Real
from typing import List, Optional, Type, Union

import pint

from cpalgprototypes.propertydescriptor import MISSING, MISSING_TYPE, PropertyDescriptor
from cpalgprototypes.units import QuantityBase, QuantityT  # type: ignore


class QuantityProperty(
    PropertyDescriptor[Union[Real, str, pint.Quantity, QuantityBase], QuantityT]
):
    """Property descriptor for physical quantities

    The property is typed by the unit it expects and the pint package takes care of the
    conversion if another value is provided
    """

    def __init__(
        self,
        unit: Type[QuantityT],
        default_value: Union[QuantityBase, MISSING_TYPE] = MISSING,
        *,
        property_name: Optional[str] = None,
        required: bool = False,
    ):
        self._unit = unit
        super().__init__(default_value, property_name=property_name, required=required)

    @property
    def unit(self) -> Type[QuantityT]:
        """The unit expected"""
        return self._unit

    def _convert_set(
        self, value: Union[Real, str, pint.Quantity, QuantityBase]
    ) -> QuantityT:
        if isinstance(value, str):
            return self.unit.parse(value)
        if isinstance(value, QuantityBase):
            value = value.quantity
        return self.unit(value)

    def _convert_copy(self, value: QuantityT) -> Real:
        return value.magnitude


class QuantityListProperty(
    PropertyDescriptor[
        List[Union[Real, str, pint.Quantity, QuantityBase]], List[QuantityT]
    ]
):
    """Property descriptor for lists of physical quantities

    The property is typed by the unit it expects and the pint package takes care of the
    conversion if another value is provided
    """

    def __init__(
        self,
        unit: Type[QuantityT],
        default_value: Union[
            List[Union[Real, str, pint.Quantity, QuantityBase]], MISSING_TYPE
        ] = MISSING,
        *,
        property_name: Optional[str] = None,
        required: bool = False,
    ):
        self._unit = unit
        super().__init__(
            default_value,  # type: ignore[arg-type]
            property_name=property_name,
            required=required,
        )

    @property
    def unit(self) -> Type[QuantityT]:
        """The unit expected"""
        return self._unit

    def _convert_set(
        self, value: List[Union[Real, str, pint.Quantity, QuantityBase]]
    ) -> List[QuantityT]:
        out: List[QuantityT] = []
        for v in value:
            if isinstance(value, str):
                out.append(self.unit.parse(v))
            else:
                if isinstance(v, QuantityBase):
                    v = v.quantity
                out.append(self.unit(v))
        return out

    def _convert_copy(self, value: List[QuantityT]) -> List[Real]:
        return [v.magnitude for v in value]
