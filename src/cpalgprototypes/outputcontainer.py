"""Handle class where a prototype produces a container"""

from __future__ import annotations
from typing import Dict, Optional, Set, Union
from componentwrapper.component import Component
from cpalggraph.configurationinfo import ConfigurationInfo

from cpalggraph.datatypes import Dependency
from cpalgprototypes.containerdependentmixin import ContainerDependentMixin
from cpalgprototypes.containerhandle import ContainerHandle
from cpalgprototypes.propertydescriptor import MISSING, MISSING_TYPE
from cpalgprototypes.prototype import Prototype


class OutputContainer(ContainerHandle, ContainerDependentMixin):
    """Handle class where a prototype produces a container

    If a parent container is set, it will be used as the parent container for the
    created container
    """

    def __init__(
        self,
        default_value: Union[str, MISSING_TYPE] = MISSING,
        *,
        is_sys_varied: bool = False,
        property_name: Optional[str] = None,
        required: bool = True,
        parent_container: Union[ContainerHandle, str, None] = None,
        literal_container: Optional[str] = None,
        force_no_parent: bool = False,
        **kwargs,
    ):
        """Create the handle

        Parameters
        ----------
        default_value : Optional[SetT], optional
            An optional default value for the property
        is_sys_varied : bool
            Whether the output container is systematically varied
        property_name : Optional[str], optional
            The property name to set. If not supplied will be set by the name of this
            attribute.
        required : bool
            Whether this property must be set for the configuration to be valid
        parent_container : ContainerHandle | str, optional
            Either the container handle for the parent container or the name of that
            container on the owner of this handle, by default None
        literal_container : str | None
            A literal container name to use. If set prefer this over parent_container
        force_no_parent : bool, optional
            Force there to be no parent container assigned, by default Fals
        """
        super().__init__(
            default_value,
            property_name=property_name,
            required=required,
            parent_container=parent_container,
            literal_container=literal_container,
            force_no_parent=force_no_parent,
            **kwargs,
        )
        self.is_sys_varied = is_sys_varied

    def produces(self, obj: Prototype) -> Set[Dependency]:
        value = self.__get__(obj)
        parent = self.get_parent_container(obj)
        # If the value is the same as the parent then we haven't produced it, just
        # updated
        if value == parent:
            return set()
        else:
            return {Dependency(value, None)}

    def produces_containers(self, obj: Prototype) -> Dict[str, Optional[str]]:
        return {self.__get__(obj): self.get_parent_container(obj)}

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        component[self.property_name] = info.get_next_container_name(
            self.__get__(obj), self.is_sys_varied
        )
