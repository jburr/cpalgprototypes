"""Helper class for declaring the 'parent' container for a tool

This is largely used by the jet tools to declare the parent container
"""

from __future__ import annotations
from typing import Optional
from cpalgprototypes.prototype import Prototype
from cpalgprototypes.containerdependentmixin import ContainerDependentMixin
from cpalgprototypes.propertydescriptor import PropertyDescriptor


class ParentContainer(PropertyDescriptor[None, str], ContainerDependentMixin):
    """Container handle for tools using a container from their parent object

    This should be used where a parent will retrieve/write the container and pass it to
    the tool. The tool never retrieves the container, but it still needs to know the
    name to declare any of its dependencies to the scheduler.
    """

    def __get__(self, obj: Optional[Prototype], objtype=None):
        if obj is None:
            return self
        else:
            assert obj.parent
            return obj.parent.assert_default_container()

    def __set__(self, obj: Prototype, value: None) -> None:
        return None

    def has_value(self, obj: Prototype) -> bool:
        return True
