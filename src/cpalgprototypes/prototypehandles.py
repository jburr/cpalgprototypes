"""Handle classes for prototypes

These ensure that the dependencies of a prototype are passed to their parents
"""

from __future__ import annotations
from typing import (
    DefaultDict,
    Dict,
    List,
    Optional,
    Sequence,
    Set,
    TypeVar,
    Union,
    cast,
)
import boolean
from componentwrapper.tool import Tool

from cpalggraph.configurationinfo import ConfigurationInfo
from cpalggraph.datatypes import Dependency
from cpalggraph.node import merge_produces_containers
from cpalggraph.preseldict import PreselDict
from cpalgprototypes.handledescriptor import HandleDescriptor
from cpalgprototypes.propertydescriptor import MISSING, MISSING_TYPE
from componentwrapper.component import Component
from cpalgprototypes.prototype import Prototype, ServicePrototype, ToolPrototype


PrototypeT = TypeVar("PrototypeT", bound=Prototype)
ToolPrototypeT = TypeVar("ToolPrototypeT", bound=ToolPrototype)
ServicePrototypeT = TypeVar("ServicePrototypeT", bound=ServicePrototype)


class PrototypeHandle(HandleDescriptor[PrototypeT, PrototypeT]):
    """Handle class for a prototype"""

    def __init__(
        self,
        default_value: Union[PrototypeT, MISSING_TYPE] = MISSING,
        *,
        property_name: Optional[str] = None,
        required: bool = False,
        rename_containers: Optional[Dict[Optional[str], Optional[str]]] = None,
    ):
        """Create the handle

        Parameters
        ----------
        rename_containers : Optional[Dict[Optional[str], Optional[str]]], optional
            Rename containers from the parent to the child prototype, by default None
        """
        super().__init__(default_value, property_name=property_name, required=required)
        if rename_containers is None:
            rename_containers = {}
        self._rename_containers = rename_containers

    def __set__(self, obj: Prototype, value: PrototypeT) -> None:
        super().__set__(obj, value)
        value.set_parent(obj, self._rename_containers)

    def produces(self, obj: Prototype) -> Set[Dependency]:
        return self.__get__(obj).produces()

    def produces_containers(self, obj: Prototype) -> Dict[str, Optional[str]]:
        return self.__get__(obj).produces_containers()

    def requires(self, obj: Prototype) -> Set[Dependency]:
        return self.__get__(obj).requires()

    def _convert_copy(self, value: PrototypeT) -> Component:
        return value.create_component()

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        self.__get__(obj).initialize_component(component[self.property_name], info)

    def finalize(
        self,
        obj: Prototype,
        component: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        self.__get__(obj).finalize_component(
            component[self.property_name],
            container_preselections,
            required_output,
            info,
        )


class PrototypeHandleArray(HandleDescriptor[List[PrototypeT], List[PrototypeT]]):
    """Handle for a list of prototypes"""

    def __init__(
        self,
        default_value: Union[List[PrototypeT], MISSING_TYPE] = MISSING,
        *,
        property_name: Optional[str] = None,
        required: bool = False,
        rename_containers: Optional[Dict[Optional[str], Optional[str]]] = None,
    ):
        """Create the handle

        Parameters
        ----------
        rename_containers : Optional[Dict[Optional[str], Optional[str]]], optional
            Rename containers from the parent to the child prototype, by default None
        """
        if default_value is MISSING:
            default_value = []
        super().__init__(default_value, property_name=property_name, required=required)

        if rename_containers is None:
            rename_containers = {}
        self._rename_containers = rename_containers

    def __set__(self, obj: Prototype, value: List[PrototypeT]) -> None:
        for v in value:
            v.set_parent(obj, self._rename_containers)
        super().__set__(obj, value)

    def produces(self, obj: Prototype) -> Set[Dependency]:
        return set().union(*(p.produces() for p in self.__get__(obj)))

    def produces_containers(self, obj: Prototype) -> Dict[str, Optional[str]]:
        return merge_produces_containers(
            p.produces_containers() for p in self.__get__(obj)
        )

    def requires(self, obj: Prototype) -> Set[Dependency]:
        return set().union(*(p.requires() for p in self.__get__(obj)))

    def _convert_copy(self, value: List[PrototypeT]) -> Sequence[Component]:
        return [v.create_component() for v in value]

    def initialize(
        self, obj: Prototype, component: Component, info: ConfigurationInfo
    ) -> None:
        for p, c in zip(self.__get__(obj), component[self.property_name]):
            p.initialize_component(c, info)

    def finalize(
        self,
        obj: Prototype,
        component: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        for p, c in zip(
            reversed(self.__get__(obj)), reversed(component[self.property_name])
        ):
            p.finalize_component(c, container_preselections, required_output, info)


class PrivateToolHandle(PrototypeHandle[ToolPrototypeT]):
    """Handle type for private tools"""

    def _convert_copy(self, value: ToolPrototypeT) -> Tool:
        tool = cast(Tool, super()._convert_copy(value))
        tool.is_public = False
        tool.in_array = False
        return tool


class PrivateToolHandleArray(PrototypeHandleArray[ToolPrototypeT]):
    """Handle type for arrays of private tools"""

    def _convert_copy(self, value: List[ToolPrototypeT]) -> Sequence[Tool]:
        tools = cast(Sequence[Tool], super()._convert_copy(value))
        for tool in tools:
            tool.is_public = False
            tool.in_array = True
        return tools


class PublicToolHandle(PrototypeHandle[ToolPrototypeT]):
    """Handle type for public tools"""

    def _convert_copy(self, value: ToolPrototypeT) -> Tool:
        tool = cast(Tool, super()._convert_copy(value))
        tool.is_public = True
        tool.in_array = False
        return tool


class PublicToolHandleArray(PrototypeHandleArray[ToolPrototypeT]):
    """Handle type for arrays of public tools"""

    def _convert_copy(self, value: List[ToolPrototypeT]) -> Sequence[Tool]:
        tools = cast(Sequence[Tool], super()._convert_copy(value))
        for tool in tools:
            tool.is_public = True
            tool.in_array = True
        return tools


class ServiceHandle(PrototypeHandle[ServicePrototypeT]):
    """Handle type for services"""


class ServiceHandleArray(PrototypeHandleArray[ServicePrototypeT]):
    """Handle type for arrays of services"""
