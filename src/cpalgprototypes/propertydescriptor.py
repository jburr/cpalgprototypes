"""Module containing python descriptors for properties and dependency handles

The python docs already contain a good description of the descriptor protocol which is
linked here: https://docs.python.org/3/howto/descriptor.html. In short, they are ways
for python classes to control how data access on instances work.

The purpose of the descriptors in this module is to provide a link between named class
attributes and the underlying properties in the represented component. The more
advanced descriptors allow these to hook into the dependency mechanism, by providing
information about which properties specify inputs and outputs. We call these advanced
descriptors 'handles'.

Descriptors are an appropriate tool here as the relationships between properties and
the containers/aux items written is described by the behaviour of the C++ class, not the
python instance. For example, the selection created by the tool of an AsgSelectionAlg
is *always* attached to the container supplied to its 'particles' property. The
descriptors provided by this package allow describing that relationship at the class
level.
"""

from __future__ import annotations

import logging
from typing import (
    Any,
    Generic,
    Optional,
    Type,
    TypeVar,
    TYPE_CHECKING,
    Union,
    cast,
    overload,
)

from componentwrapper.component import Component

if TYPE_CHECKING:
    from cpalgprototypes.prototype import Prototype


log = logging.getLogger(__name__)

#: The type set on a property
SetT = TypeVar("SetT")
#: The type retrieved from a property
GetT = TypeVar("GetT")
#: Self-type for property descriptors
SelfT = TypeVar("SelfT", bound="PropertyDescriptor")


class MISSING_TYPE:
    """Sentinel object to detect if value has been set"""

    @property
    def symbol(self) -> str:
        return "cpalgnodes.propertydescriptor.MISSING"

    def __repr__(self) -> str:
        return self.symbol


MISSING = MISSING_TYPE()


class PropertyDescriptor(Generic[SetT, GetT]):
    """Descriptor that wraps property values

    Setting and retrieving the instance attributes edits the properties dictionary.

    The two type arguments reflect the two different operations the descriptor
    handles. For most simple property types they will all be the same

    SetT
        This is the type of argument that the user should provide when setting the
        attribute
    GetT
        This is the type of argument that is returned when retrieving the attribute.
        It should usually be a subset of SetT

    More complicated interactions are controlled by overriding the _convert_set and
    _convert_copy methods.
    """

    def __init__(
        self,
        default_value: Union[SetT, MISSING_TYPE] = MISSING,
        *,
        property_name: Optional[str] = None,
        required: bool = True,
        **kwargs,
    ):
        """Create the descriptor

        Parameters
        ----------
        default_value : Optional[SetT], optional
            An optional default value for the property
        property_name : Optional[str], optional
            The property name to set. If not supplied will be set by the name of this
            attribute.
        required : bool
            Whether this property must be set for the configuration to be valid
        """
        super().__init__(**kwargs)
        self._property_name = property_name
        self._default_value = default_value
        self._required = required

    def __set_name__(self, owner: Type[Prototype], name: str) -> None:
        if self._property_name is None:
            self._property_name = name

    @property
    def has_default(self) -> bool:
        """Does this property have a default value"""
        return self._default_value is not MISSING

    @property
    def required(self) -> bool:
        """Is this property required?"""
        return self._required

    def has_value(self, obj: Prototype) -> bool:
        """Is there a valid value for this property (default or explicitly set)"""
        return self._property_name in obj

    @property
    def property_name(self) -> str:
        """The name of this property on the parent"""
        assert self._property_name
        return self._property_name

    def _convert_set(self, value: SetT) -> GetT:
        """Convert to the type stored on the prototype

        This is called when the value is set on the parent object to convert the set
        value to the value stored in the prototype's dictionary
        """
        return cast(GetT, value)

    def _convert_copy(self, value: GetT) -> Any:
        """Convert to the type stored on the component

        This is called when the property value is copied to the actual Component object
        """
        return value

    def copy_prop(self, obj: Prototype, component: Component) -> None:
        component[self.property_name] = self._convert_copy(self.__get__(obj))

    @overload
    def __get__(
        self,
        obj: Prototype,
        objtype: Optional[Type[Prototype]] = None,
    ) -> GetT:
        ...

    @overload
    def __get__(self: SelfT, obj: None, objtype: Type[Prototype]) -> SelfT:
        ...

    def __get__(self, obj: Optional[Prototype], objtype=None):
        if obj is None:
            return self
        else:
            try:
                return obj[self.property_name]
            except KeyError as e:
                raise AttributeError from e

    def __set__(self, obj: Prototype, value: SetT) -> None:
        obj[self.property_name] = self._convert_set(value)
