"""Base class for handles which interact with a container"""

from __future__ import annotations
from typing import Any, DefaultDict, Optional, Union

import boolean

from componentwrapper.component import Component
from cpalggraph.preseldict import PreselDict
from cpalggraph.configurationinfo import ConfigurationInfo

from cpalgprototypes.handledescriptor import HandleDescriptor
from cpalgprototypes.propertydescriptor import MISSING, MISSING_TYPE
from cpalgprototypes.prototype import Prototype


class ContainerHandle(HandleDescriptor[str, str]):
    """Base class for handles which describe a container

    Containers are stored on the prototypes by their nicknames. The correct storegate
    name is calculated and stored on the components.
    """

    def __init__(
        self,
        default_value: Union[str, MISSING_TYPE] = MISSING,
        *,
        is_default_container: bool = False,
        property_name: Optional[str] = None,
        required: bool = False,
        **kwargs: Any,
    ) -> None:
        """Create the handle

        Parameters
        ----------
        is_default_container : bool, optional
            Is this the default container for this prototype, by default False
        """
        super().__init__(
            default_value, property_name=property_name, required=required, **kwargs
        )
        self.is_default_container = is_default_container

    def finalize(
        self,
        obj: Prototype,
        copy: Component,
        container_preselections: DefaultDict[str, boolean.Expression],
        required_output: PreselDict,
        info: ConfigurationInfo,
    ) -> None:
        # Finalize the container name (remove the final temporary container name)
        copy[self.property_name] = info.replace_final_container_name(
            self.__get__(obj), copy[self.property_name]
        )
        super().finalize(obj, copy, container_preselections, required_output, info)
